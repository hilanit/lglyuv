public class Cycle6 {
    public static void main(String[] args) {
        long a = 673;
        long result = 0;
        while (a != 0) {
            result = a % 10;
            a /= 10;
            System.out.print(result);
        }
    }
}

