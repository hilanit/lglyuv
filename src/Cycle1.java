public class Cycle1 {
    public static void main(String[] args) {
        int sum = 0;
        int kol = 0;

        for (int a = 1; a <= 99; a++) {
            if (a % 2 == 0) {
                kol++;
                sum += a;
            }
        }
        System.out.println("Сума чётных чисел: " + sum);

    }
}
