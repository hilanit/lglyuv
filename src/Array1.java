public class Array1 {
    public static void main(String[] args){
        int[] array={1,41,463,45,2,143,535,332,654,25};
        int minValue = array[0];
        for (int i = 0; i< array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            }
        }
        System.out.println("minValue: " + minValue);
    }
}
