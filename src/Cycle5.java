public class Cycle5 {
    public static void main(String[] args) {
        long a = 123;
        long sum=0;
        while (a > 0) {
            sum +=(a%10);
            a /= 10;
        }
        System.out.println(sum+" ");
    }
}
