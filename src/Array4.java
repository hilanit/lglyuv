public class Array4 {
    public static void main(String[] args) {
        int[] array=new int[] { 1, 41, 463, 45, 2, 143, 535, 332, 654, 25};
        int maxValue = array[0];
        int maxIndex = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        System.out.println("maxIndex:" + maxIndex);
    }
}