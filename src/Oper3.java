public class Oper3 {
    public static void main(String[] args) {
        float a = 875.564f;
        float b = 984.903f;
        float c = -535.593f;
        float sum;
        if (a > 0 && b > 0 && c > 0) {
            sum = a + b + c;
        } else if (a <= 0 && b > 0 && c > 0) {
            sum = b + c;
        } else if (a > 0 && b <= 0 && c > 0) {
            sum = a + c;
        } else if (a > 0 && b > 0 && c <= 0) {
            sum = a + b;
        } else if (a <= 0 && b > 0 && c <= 0) {
            sum = b;
        } else if (a > 0 && b <= 0 && b <= 0) {
            sum = a;
        } else {
            sum = c;
        }
        System.out.println(sum);
    }

}
