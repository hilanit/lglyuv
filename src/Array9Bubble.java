public class Array9Bubble {
    public static void main(String[] args) {
        int[] array = new int[]{7, 13, 5, 17, 23, 48};
        boolean isSorted = false;
        int buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < array.length-1; i++) {
                if(array[i] > array[i+1]){
                    isSorted = false;

                    buf = array[i];
                    array[i] = array[i+1];
                    array[i+1] = buf;
                }
            }
        }
        for (int j=0; j< array.length; j++){
            System.out.print(array[j]+" ");
        }
    }
}