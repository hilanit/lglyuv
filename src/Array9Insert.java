public class Array9Insert {
    public static void main(String[] args) {
        int[] array = new int[]{40, 34, 21, 54, 65, 78, 12, 9, 1, 2, 36, 76, 100, 15, 235, 33, 23, 20};
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > current) {
                array[j] = array[j - 1];
                j --;
            }
            array[j] = current;
        }
        for (int l = 0; l < array.length; l++) {
            System.out.print(array[l]+" ");
        }
    }
}