public class Array8 {
    public static void main(String[] args) {
        int[] array;
        array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 22, 33, 44, 55, 66, 77, 88, 99};
        int mod = array.length % 2;

        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length / 2 + i + mod];
            array[array.length / 2 + i + mod] = temp;
        }
        for (int j=0; j < array.length; j++){
            System.out.print(array[j]+" ");
        }
    }
}
