public class Array9Select {
    public static void main (String[] args){
        int[] array= new int[]{40,34,21,54,65,78,12,9,1,2,36,76,100,15,235,33,23,20};
     for (int i = 0; i < array.length; i++) {

        int minValue = array[i];
        int minIndex= i;

        for (int j = i + 1; j < array.length; j++) {
            if (array[j] < minValue) {
                minValue = array[j];
                minIndex = j;
            }
        }
        if (i != minIndex) {
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        System.out.print(array[i] + " ");
    }
}
}
