public class Array7 {
    static public void main(String[] args) {
        int[] array;
        array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        int kol = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                kol++;
            }
        }
        System.out.println("Количество нечётных элементов:" + kol);
    }
}